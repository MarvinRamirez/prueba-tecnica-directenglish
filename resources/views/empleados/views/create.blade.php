 @extends('empleados.layout')
 @section('content')
 <div class="alert alert-primary alert-dismissible fade show data-alert" role='alert' style="display: none;">
						{{-- {{ session('flash') }} --}}
						<button type="button" class="close" data-dismiss="alert" aria-label="close" >
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
 <form method="POST" action="{{ route('empleados.store') }}" id="saveforms">
  @csrf
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
	    <div class="modal-header">

	      <h4 class="modal-title" id="myModalLabel">Creacion de un registro</h4>
	    </div>
	    <div class="modal-body">
	      <div class="form-group {{ $errors->has('nombre_empleado') ? 'has-error' : '' }}">
	        <input id="nombre" class="form-control" type="text" name="nombre_empleado" placeholder="ingrese el nombre del empleado"
	        value="{{ old('nombre_empleado')}}" autofocus >
	        {!! $errors->first('nombre_empleado', '<span class="help-block">:message</span>') !!}
	      </div>

	      <div class="form-group {{ $errors->has('apellido_empleado') ? 'has-error' : '' }}">
	        <input id="apellido" class="form-control" type="text" name="apellido_empleado" placeholder="ingrese el apellido del empleado"
	        value="{{ old('apellido_empleado')}}" autofocus >
	        {!! $errors->first('apellido_empleado', '<span class="help-block">:message</span>') !!}
	      </div>

	      <div class="form-group {{ $errors->has('direccion_empleado') ? 'has-error' : '' }}">
	        <input id="direccion_empleado" class="form-control" type="text" name="direccion_empleado" placeholder="ingrese la direccion del empleado"
	        value="{{ old('direccion_empleado')}}" autofocus >
	        {!! $errors->first('direccion_empleado', '<span class="help-block">:message</span>') !!}
	      </div>

	      <div class="form-group {{ $errors->has('telefono_empleado') ? 'has-error' : '' }}">
	        <input id="telefono_empleado" class="form-control" type="text" name="telefono_empleado" placeholder="ingrese el telefono del empleado"
	        value="{{ old('telefono_empleado')}}" autofocus >
	        {!! $errors->first('telefono', '<span class="help-block">:message</span>') !!}
	      </div>

	      <div class="form-group {{ $errors->has('empresa_id') ? 'has-error' : '' }}">
	      	<label>Seleccione la empresa</label>
	        <select class="form-control" name="empresa_id">
	        	@foreach($empresas as $empresa)
	        		<option value="{{ $empresa->id }}">
	        			{{ $empresa->nombre_empresa }}
	        		</option>
	        	@endforeach
	        </select>
	      </div>
	    </div>
	    <div class="modal-footer">
	      <button id="createpost"  onclick="saveproject()"  class="btn btn-primary">Crear publicacion</button>
	    </div>
	  </div>
	</div>
</form>
@endsection