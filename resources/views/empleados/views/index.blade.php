@extends('empleados.layout')
@section('content')
	<div class="container">
		<div class="row">
              @if(session('flash'))
					<div class="alert alert-primary alert-dismissible fade show data-alert" role='alert' >
						{{ session('flash') }}
						<button type="button" class="close" data-dismiss="alert" aria-label="close" >
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
			<div class="mx-auto">
				CONTENIDO DE LA TABLA EMPLEADOS
			</div>
		</div>
		<div class="row">
			<div class="mx-auto">
				<a  href="{{ route('empleados.create')}}">Crear un empleado</a>
				<table class="table">
					<tr>
						<td>ID_EMPLEADO</td>
						<td>NOMBRE</td>
						<td>APELLIDO</td>
						<td>DIRECCION</td>
						<td>TELEFONO</td>
						<td>EMPRESA</td>
						<td>ACCIONES</td>
					</tr>
					@foreach($empleados as $empleado)
						<tr>
							<td>{{$empleado->id}}</td>
							<td>{{$empleado->nombre_empleado}}</td>
							<td>{{$empleado->apellido_empleado}}</td>
							<td>{{$empleado->direccion_empleado}}</td>
							<td>{{$empleado->telefono_empleado}}</td>
							<td>{{$empleado->empresa->nombre_empresa}}</td>
							<td>
								<a class="btn btn-xs  btn-primary" href="{{ route('empleados.edit',$empleado) }}">Editar</a>

								/
								<form id="saveformdelete" action="{{ route('empleados.destroy',$empleado) }}" method="POST" style="display: inline;">
				                @csrf @method('DELETE')
				                <button onclick="return confirm('Esta seguro de eliminar la prublicacion ?')"class="btn btn-xs  btn-danger">Eliminar</button>
				              </form>
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
@endsection