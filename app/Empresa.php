<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
	protected $fillable =
					[
						'nombre_empresa','direccion_empresa','telefono_empresa','activo'
					];
    public function empleados(){
    	return $this->hasMany(Empleado::class);
    }
    public function activate(){
        $this->update(['activo' => false]);
    }
}
