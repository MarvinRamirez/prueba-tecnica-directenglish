<?php

namespace App\Http\Controllers;

use App\Empresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('empresas.views.index',[
            'empresas' => Empresa::with('empleados')->where('activo','1')->latest()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('empresas.views.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request,[
            'nombre_empresa' => 'required',
            'direccion_empresa' => 'required',
            'telefono_empresa' => 'required',
        ]);
        Empresa::create($validate);
        return redirect()->route('empresas.index')->withFlash('Dato guardado exitosamnete');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function show(Empresa $empresa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function edit(Empresa $empresa)
    {
         return view('empresas.views.edit',[
            'empresa' => $empresa
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empresa $empresa)
    {
        if($request->ajax()){
            $validate = $this->validate($request,[
                'nombre_empresa' => 'required',
                'direccion_empresa' => 'required',
                'telefono_empresa' => 'required',
            ]);

            $empresa->update($validate);
            return response()->json([
                "mensaje" => "datos actualizados has sido actualizados con exito redirigir a <a href='http://www.directenglish.local/empresas'>Home</>"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empresa $empresa)
    {
         $empresa->activate();
        return redirect()->route('empresas.index')->withFlash("El registro ha sido eliminado con exito");
    }
}
