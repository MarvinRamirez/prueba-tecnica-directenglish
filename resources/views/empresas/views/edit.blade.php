 @extends('empresas.layout')
 @section('content')
 <form method="POST" action="#" id="saveformsupdate">
  @csrf
  @method('PUT')
  <div class="alert alert-primary alert-dismissible fade show data-alert" role='alert' style="display: none;" >
		<button type="button" class="close" data-dismiss="alert" aria-label="close" >
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
	    <div class="modal-header">

	      <h4 class="modal-title" id="myModalLabel">Editar un proyecto</h4>
	    </div>
	    <div class="modal-body">
	      <div class="form-group {{ $errors->has('nombre_empresa') ? 'has-error' : '' }}">
	        <input id="nombre" class="form-control" type="text" name="nombre_empresa" placeholder="ingrese el nombre de la empresa"
	        value="{{ old('nombre_empresa',$empresa->nombre_empresa)}}" autofocus >
	        {!! $errors->first('nombre_empresa', '<span class="help-block">:message</span>') !!}
	      </div>

	      <div class="form-group {{ $errors->has('direccion_empresa') ? 'has-error' : '' }}">
	        <input id="direccion_empresa" class="form-control" type="text" name="direccion_empresa" placeholder="ingrese la direccion de la empresa"
	        value="{{ old('direccion_empresa',$empresa->direccion_empresa)}}" autofocus >
	        {!! $errors->first('direccion_empresa', '<span class="help-block">:message</span>') !!}
	      </div>

	      <div class="form-group {{ $errors->has('telefono_empresa') ? 'has-error' : '' }}">
	        <input id="telefono_empresa" class="form-control" type="text" name="telefono_empresa" placeholder="ingrese el telefono del empleado"
	        value="{{ old('telefono_empresa',$empresa->telefono_empresa)}}" autofocus >
	        {!! $errors->first('telefono_empresa', '<span class="help-block">:message</span>') !!}
	      </div>
	    </div>
	    <div class="modal-footer">
	      <button  class="btn btn-primary" onclick="actualizar({{ $empresa->id }})">Actualizar publicacion</button>
	    </div>
	  </div>
	</div>
</form>
@endsection