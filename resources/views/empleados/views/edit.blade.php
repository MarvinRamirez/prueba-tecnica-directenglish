 @extends('empleados.layout')
 @section('content')
 <form method="POST" action="#" id="saveformsupdate">
  @csrf
  @method('PUT')
  <div class="alert alert-primary alert-dismissible fade show data-alert" role='alert' style="display: none;" >
		<button type="button" class="close" data-dismiss="alert" aria-label="close" >
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
	    <div class="modal-header">

	      <h4 class="modal-title" id="myModalLabel">Editar un proyecto</h4>
	    </div>
	    <div class="modal-body">
	      <div class="form-group {{ $errors->has('nombre_empleado') ? 'has-error' : '' }}">
	        <input id="nombre" class="form-control" type="text" name="nombre_empleado" placeholder="ingrese el nombre del empleado"
	        value="{{ old('nombre_empleado',$empleado->nombre_empleado)}}" autofocus >
	        {!! $errors->first('nombre_empleado', '<span class="help-block">:message</span>') !!}
	      </div>

	      <div class="form-group {{ $errors->has('apellido_empleado') ? 'has-error' : '' }}">
	        <input id="apellido" class="form-control" type="text" name="apellido_empleado" placeholder="ingrese el apellido del empleado"
	        value="{{ old('apellido_empleado',$empleado->apellido_empleado)}}" autofocus >
	        {!! $errors->first('apellido_empleado', '<span class="help-block">:message</span>') !!}
	      </div>

	      <div class="form-group {{ $errors->has('direccion_empleado') ? 'has-error' : '' }}">
	        <input id="direccion_empleado" class="form-control" type="text" name="direccion_empleado" placeholder="ingrese la direccion del empleado"
	        value="{{ old('direccion_empleado',$empleado->direccion_empleado)}}" autofocus >
	        {!! $errors->first('direccion_empleado', '<span class="help-block">:message</span>') !!}
	      </div>

	      <div class="form-group {{ $errors->has('telefono_empleado') ? 'has-error' : '' }}">
	        <input id="telefono_empleado" class="form-control" type="text" name="telefono_empleado" placeholder="ingrese el telefono del empleado"
	        value="{{ old('telefono_empleado',$empleado->telefono_empleado)}}" autofocus >
	        {!! $errors->first('telefono', '<span class="help-block">:message</span>') !!}
	      </div>

	      <div class="form-group {{ $errors->has('empresa_id') ? 'has-error' : '' }}">
	      	<label>Seleccione la empresa</label>
	        <select class="form-control" name="empresa_id">
	        	@foreach($empresa as $id =>$name)
	        		<option value="{{ $id }}" {{$id ==old('empresa_id',$empleado->empresa_id)  ? 'selected' : ''}}>
	        			{{ $name}}
	        		</option>
	        	@endforeach
	        </select>
	      </div>
	    </div>
	    <div class="modal-footer">
	      <button  class="btn btn-primary" onclick="actualizar({{$empleado->id}})">Actualizar publicacion</button>
	    </div>
	  </div>
	</div>
</form>
@endsection{{-- {{$project->id}} como parametro en el evento --}}