<?php

namespace App\Http\Controllers;

use App\Empleado;
use \App\Empresa;
use Illuminate\Http\Request;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /*return view('empleados.views.index',[
            'data' => Empleado::with('empresa')->latest()->get(),
        ]);*/
        return view('empleados.views.index',[
            'empleados' => Empleado::with('empresa')->where('activo','1')->latest()->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empleados.views.create',[
            'empresas' => Empresa::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request,[
            'nombre_empleado' => 'required',
            'apellido_empleado' => 'required',
            'direccion_empleado' => 'required',
            'telefono_empleado' => 'required',
            'empresa_id' => 'required',
        ]);
        Empleado::create($validate);
        return redirect()->route('empleados.index')->withFlash('Dato guardado exitosamnete');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function show(Empleado $empleado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function edit(Empleado $empleado)
    {
        return view('empleados.views.edit',[
            'empresa' => Empresa::pluck('nombre_empresa','id'),
            'empleado' => $empleado
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empleado $empleado)
    {
        if($request->ajax()){
            $validate = $this->validate($request,[
                'nombre_empleado' => 'required',
                'apellido_empleado' => 'required',
                'direccion_empleado' => 'required',
                'telefono_empleado' => 'required',
                'empresa_id' => 'required',
            ]);

            $empleado->update($validate);
            return response()->json([
                "mensaje" => "datos actualizados has sido actualizados con exito redirigir a <a href='http://www.directenglish.local/empleados'>Home</>"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empleado $empleado)
    {
         $empleado->activate();
        return redirect()->route('empleados.index')->withFlash("El registro ha sido eliminado con exito");
    }
}
