<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
	protected $fillable =
					[
						'nombre_empleado','apellido_empleado','direccion_empleado','telefono_empleado','empresa_id','activo'
					];
    public function empresa(){
    	return $this->belongsTo(Empresa::class);
    }
    public function activate(){
        $this->update(['activo' => false]);
    }
}
