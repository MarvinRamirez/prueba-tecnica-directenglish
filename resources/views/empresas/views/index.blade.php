@extends('empresas.layout')
@section('content')
<div class="container">
		<div class="row">
              @if(session('flash'))
					<div class="alert alert-primary alert-dismissible fade show data-alert" role='alert' >
						{{ session('flash') }}
						<button type="button" class="close" data-dismiss="alert" aria-label="close" >
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
			<div class="mx-auto">
				CONTENIDO DE LA TABLA EMPRESAS
			</div>
		</div>
		<div class="row">
			<div class="mx-auto">
				<a  href="{{ route('empresas.create')}}">Crear una empresa</a>
				<table class="table">
					<tr>
						<td>ID_EMPLEADO</td>
						<td>NOMBRE</td>
						<td>DIRECCION</td>
						<td>TELEFONO</td>>
						<td>ACCIONES</td>
					</tr>
					@foreach($empresas as $empresa)
						<tr>
							<td>{{$empresa->id}}</td>
							<td>{{$empresa->nombre_empresa}}</td>
							<td>{{$empresa->direccion_empresa}}</td>
							<td>{{$empresa->telefono_empresa}}</td>
							<td>
								<a class="btn btn-xs  btn-primary" href="{{ route('empresas.edit',$empresa) }}">Editar</a>

								/
								<form id="saveformdelete" action="{{ route('empresas.destroy',$empresa) }}" method="POST" style="display: inline;">
				                @csrf @method('DELETE')
				                <button onclick="return confirm('Esta seguro de eliminar la prublicacion ?')"class="btn btn-xs  btn-danger">Eliminar</button>
				              </form>
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
</div>
@endsection