<?php

use \App\Empresa;
use Illuminate\Database\Seeder;

class EmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empresa::truncate();
        $empresa = new Empresa;
        $empresa->nombre_empresa = "DirectEnglish";
        $empresa->direccion_empresa = "San Salvador";
        $empresa->telefono_empresa = "45678976";
        $empresa->save();

        $empresa = new Empresa;
        $empresa->nombre_empresa = "Claro";
        $empresa->direccion_empresa = "San Salvador";
        $empresa->telefono_empresa = "42278976";
        $empresa->save();

        $empresa = new Empresa;
        $empresa->nombre_empresa = "Tigo";
        $empresa->direccion_empresa = "San Salvador";
        $empresa->telefono_empresa = "12345678";
        $empresa->save();
    }
}
