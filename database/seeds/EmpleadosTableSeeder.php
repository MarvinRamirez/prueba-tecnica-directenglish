<?php

use \App\Empleado;
use Illuminate\Database\Seeder;

class EmpleadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empleado::truncate();
        $empleado = new Empleado;
        $empleado->nombre_empleado = "Marvin";
        $empleado->apellido_empleado = "Ramirez";
        $empleado->direccion_empleado = "San Salvador";
        $empleado->telefono_empleado = "45678976";
        $empleado->empresa_id = "1";
        $empleado->activo = true;
        $empleado->save();

        $empleado = new Empleado;
        $empleado->nombre_empleado = "Ana";
        $empleado->apellido_empleado = "Peralta";
        $empleado->direccion_empleado = "San Salvador";
        $empleado->telefono_empleado = "45678976";
        $empleado->empresa_id = "2";
        $empleado->activo = true;
        $empleado->save();

        $empleado = new Empleado;
        $empleado->nombre_empleado = "Guadalupe";
        $empleado->apellido_empleado = "Mejia";
        $empleado->direccion_empleado = "San Salvador";
        $empleado->telefono_empleado = "45678976";
        $empleado->empresa_id = "3";
        $empleado->activo = true;
        $empleado->save();

    }
}
