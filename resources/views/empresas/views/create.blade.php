 @extends('empresas.layout')
 @section('content')
 <div class="alert alert-primary alert-dismissible fade show data-alert" role='alert' style="display: none;">
	{{-- {{ session('flash') }} --}}
	<button type="button" class="close" data-dismiss="alert" aria-label="close" >
		<span aria-hidden="true">&times;</span>
	</button>
</div>
 <form method="POST" action="{{ route('empresas.store') }}" id="saveforms">
  @csrf
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
	    <div class="modal-header">

	      <h4 class="modal-title" id="myModalLabel">Creacion de un registro</h4>
	    </div>
	    <div class="modal-body">
	      <div class="form-group {{ $errors->has('nombre_empresa') ? 'has-error' : '' }}">
	        <input id="nombre" class="form-control" type="text" name="nombre_empresa" placeholder="ingrese el nombre de la empresa"
	        value="{{ old('nombre_empresa')}}" autofocus >
	        {!! $errors->first('nombre_empresa', '<span class="help-block">:message</span>') !!}
	      </div>

	      <div class="form-group {{ $errors->has('direccion_empresa') ? 'has-error' : '' }}">
	        <input id="direccion_empresa" class="form-control" type="text" name="direccion_empresa" placeholder="ingrese la direccion de la empresa"
	        value="{{ old('direccion_empresa')}}" autofocus >
	        {!! $errors->first('direccion_empresa', '<span class="help-block">:message</span>') !!}
	      </div>

	      <div class="form-group {{ $errors->has('telefono_empresa') ? 'has-error' : '' }}">
	        <input id="telefono_empresa" class="form-control" type="text" name="telefono_empresa" placeholder="ingrese el telefono del empleado"
	        value="{{ old('telefono_empresa')}}" autofocus >
	        {!! $errors->first('telefono_empresa', '<span class="help-block">:message</span>') !!}
	      </div>
	    </div>
	    <div class="modal-footer">
	      <button id="createpost"  onclick="saveproject()"  class="btn btn-primary">Crear publicacion</button>
	    </div>
	  </div>
	</div>
</form>
@endsection